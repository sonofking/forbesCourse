package com.example.demo.controller;

import com.example.demo.domain.Comment;
import com.example.demo.repo.CommentRepo;
import lombok.RequiredArgsConstructor;
import org.apache.tomcat.jni.Local;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/comment")
public class CommentController {
    private final CommentRepo commentRepo;

    @GetMapping
    public List<Comment> list(){
        return commentRepo.findAll();
    }

    @PostMapping
    public Comment create(@RequestBody Comment comment){
        comment.setCreateDate(LocalDateTime.now());
        return commentRepo.save(comment);
    }
}
