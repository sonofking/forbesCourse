package com.example.demo.controller;

import com.example.demo.domain.Lead;
import com.example.demo.repo.LeadRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/lead")
public class LeadController {
    private final LeadRepo leadRepo;

    @GetMapping
    public List<Lead> list(){
        return leadRepo.findAll();
    }

    @PostMapping
    public Lead create(@RequestBody Lead lead){
        lead.setCreateDate(LocalDateTime.now());
        return leadRepo.save(lead);
    }
}
